addSbtPlugin("com.typesafe.sbt" % "sbt-twirl" % "1.3.13")
addSbtPlugin("org.scalatra.sbt" % "sbt-scalatra" % "1.0.2")

// Deploying to Heroku http://scalatra.org/guides/2.4/deployment/heroku.html
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.2")
