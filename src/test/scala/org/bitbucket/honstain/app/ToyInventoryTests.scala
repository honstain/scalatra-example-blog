package org.bitbucket.honstain.app

import org.bitbucket.honstain.PostgresSpec
import org.json4s.DefaultFormats
import org.scalatra.test.scalatest._
import org.json4s.jackson.Serialization.write
import org.scalatest.BeforeAndAfter
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class ToyInventoryTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  implicit val formats = DefaultFormats

  addServlet(new ToyInventory(database), "/*")

  def createInventoryTable: DBIO[Int] =
    sqlu"""
        CREATE TABLE inventory_single
        (
          id bigserial NOT NULL,
          sku text,
          qty integer,
          location text,
          CONSTRAINT pk PRIMARY KEY (id),
          UNIQUE (sku, location)
        )
      """
  def dropInventoryTable: DBIO[Int] = sqlu"DROP TABLE IF EXISTS inventory_single"

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  test("GET / on ToyInventory should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

  test("GET / on ToyInventory with no inventory should return empty list") {
    get("/") {
      body should equal (write(List()))
    }
  }

  test("GET / on ToyInventory should return inventory list") {
    post("/", write(Inventory("ZL104", 3, "Pine Block of wood"))) {}
    get("/") {
      body should equal (write(List(Inventory("ZL104", 3, "Pine Block of wood"))))
    }
  }

  test("POST / on ToyInventory should return status 200") {
    post("/", write(Inventory("ZL104", 3, "Pine Block of wood"))) {
      status should equal (200)
    }
  }
}