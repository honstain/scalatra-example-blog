package org.bitbucket.honstain.dao

import org.bitbucket.honstain.PostgresSpec
import org.scalatest.BeforeAndAfter
import org.scalatra.test.scalatest._
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class InventorySingleRecordDaoTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  def createInventoryTable: DBIO[Int] =
    sqlu"""
          CREATE TABLE inventory_single
          (
            id bigserial NOT NULL,
            sku text,
            qty integer,
            location text,
            CONSTRAINT pk PRIMARY KEY (id)
          );
      """
  def dropInventoryTable: DBIO[Int] = sqlu"DROP TABLE IF EXISTS inventory_single;"

  def createInventoryHelper(sku: String, location: String, qty: Int = 1): InventorySingleRecord = {
    val create = InventorySingleRecordDao.create(database, sku, qty, location)
    Await.result(create, Duration.Inf).get
  }

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  test("findBySku for nothing found") {
    val future = InventorySingleRecordDao.findBySku(database, "NotFoundSku")
    val result: Seq[InventorySingleRecord] = Await.result(future, Duration.Inf)

    result should equal(List())
  }

  test("findBySku for single SKU") {
    val TEST_SKU = "NewSku"
    val createdBin0 = createInventoryHelper(TEST_SKU, "Bin0")
    val createdBin1 = createInventoryHelper(TEST_SKU, "Bin1")

    val future = InventorySingleRecordDao.findBySku(database, TEST_SKU)
    val result: Seq[InventorySingleRecord] = Await.result(future, Duration.Inf)

    result should equal(List(createdBin0, createdBin1))
  }

  test("create") {
    val future = InventorySingleRecordDao.create(database, "NewSku", 1, "Bin0")
    val result: Option[InventorySingleRecord] = Await.result(future, Duration.Inf)


    result.get.sku should equal("NewSku")
    result.get.qty should equal(1)

    // I really didn't want to match on the id (since the DB sets it as auto-increment)
    result should equal(Some(InventorySingleRecord(Some(1), "NewSku", 1, "Bin0")))
  }

  test("transfer") {
    val TEST_SKU = "NewSku"
    val BIN_01 = "Bin-01"
    val BIN_02 = "Bin-02"
    createInventoryHelper(TEST_SKU, BIN_01)
    createInventoryHelper(TEST_SKU, BIN_02, 0)

    val futureTrans = InventorySingleRecordDao.transfer(database, TEST_SKU, 1, BIN_01, BIN_02)
    val transResult = Await.result(futureTrans, Duration.Inf)

    val futureFind = InventorySingleRecordDao.findBySku(database, TEST_SKU)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should contain only (
      InventorySingleRecord(Some(1), "NewSku", 0, BIN_01),
      InventorySingleRecord(Some(2), "NewSku", 1, BIN_02),
    )
  }

  test("transfer fail for insufficient funds") {
    val TEST_SKU = "NewSku"
    val BIN_01 = "Bin-01"
    val BIN_02 = "Bin-02"
    createInventoryHelper(TEST_SKU, BIN_01)

    val futureTrans = InventorySingleRecordDao.transfer(database, TEST_SKU, 2, BIN_01, BIN_02)
    val error = assertThrows[Exception](Await.result(futureTrans, Duration.Inf))

    val futureFind = InventorySingleRecordDao.findBySku(database, TEST_SKU)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should contain only InventorySingleRecord(Some(1), "NewSku", 1, BIN_01)
  }

  test("findAll") {
    val TEST_SKU = "NewSku"
    val BIN_01 = "Bin-01"
    val BIN_02 = "Bin-02"
    createInventoryHelper(TEST_SKU, BIN_01)
    createInventoryHelper(TEST_SKU, BIN_02)
    createInventoryHelper(TEST_SKU, BIN_01)
    createInventoryHelper(TEST_SKU, BIN_01)

    val futureFind = InventorySingleRecordDao.findAll(database)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should contain only (
      InventorySingleRecord(Some(1), TEST_SKU, 3, BIN_01),
      InventorySingleRecord(Some(2), TEST_SKU, 1, BIN_02),
    )
  }
}