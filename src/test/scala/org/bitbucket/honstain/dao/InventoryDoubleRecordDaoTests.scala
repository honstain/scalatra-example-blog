package org.bitbucket.honstain.dao

import org.bitbucket.honstain.PostgresSpec

import org.scalatest.BeforeAndAfter
import org.scalatra.test.scalatest._
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class InventoryDoubleRecordDaoTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  def createInventoryTable: DBIO[Int] =
    sqlu"""
          CREATE TABLE inventory
          (
            id bigserial NOT NULL,
            sku text,
            qty integer,
            type text,
            location text,
            CONSTRAINT pk PRIMARY KEY (id)
          );

          CREATE TABLE inventory_lock
          (
            location text,
            sku text,
            revision integer,
            CONSTRAINT pk_lock PRIMARY KEY (location, sku)
          );
      """
  def dropInventoryTable: DBIO[Int] = sqlu"DROP TABLE IF EXISTS inventory; DROP TABLE IF EXISTS inventory_lock;"

  def createInventoryHelper(sku: String, location: String, qty: Int = 1): InventoryDoubleRecord = {
    val create = InventoryDoubleRecordDao.create(database, sku, qty, location)
    Await.result(create, Duration.Inf)
  }

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  test("findBySku for nothing found") {
    val future = InventoryDoubleRecordDao.findBySku(database, "NotFoundSku")
    val result: Seq[InventoryDoubleRecord] = Await.result(future, Duration.Inf)

    result should equal(List())
  }

  test("findBySku for single SKU") {
    val TEST_SKU = "NewSku"
    val createdBin0 = createInventoryHelper(TEST_SKU, "Bin0")
    val createdBin1 = createInventoryHelper(TEST_SKU, "Bin1")

    val future = InventoryDoubleRecordDao.findBySku(database, TEST_SKU)
    val result: Seq[InventoryDoubleRecord] = Await.result(future, Duration.Inf)

    result should equal(List(createdBin0, createdBin1))
  }

  test("create") {
    val future = InventoryDoubleRecordDao.create(database, "NewSku", 1, "Bin0")
    val result: InventoryDoubleRecord = Await.result(future, Duration.Inf)

    result.sku should equal("NewSku")
    result.qty should equal(1)

    // I really didn't want to match on the id (since the DB sets it as auto-increment)
    result should equal(InventoryDoubleRecord(Some(1), "NewSku", 1, TRANSACTION.ADJUST, "Bin0"))
  }

  test("transfer") {
    val TEST_SKU = "NewSku"
    val BIN_01 = "Bin-01"
    val BIN_02 = "Bin-02"
    createInventoryHelper(TEST_SKU, BIN_01)

    val futureTrans = InventoryDoubleRecordDao.transfer(database, TEST_SKU, 1, BIN_01, BIN_02)
    val transResult = Await.result(futureTrans, Duration.Inf)

    val futureFind = InventoryDoubleRecordDao.findBySku(database, TEST_SKU)
    val findResult: Seq[InventoryDoubleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should equal(List(
      InventoryDoubleRecord(Some(1), "NewSku", 1, TRANSACTION.ADJUST, BIN_01),
      InventoryDoubleRecord(Some(2), "NewSku", -1, TRANSACTION.TRANSFER, BIN_01),
      InventoryDoubleRecord(Some(3), "NewSku", 1, TRANSACTION.TRANSFER, BIN_02),
    ))
  }

  test("transfer fail for insufficient funds") {
    val TEST_SKU = "NewSku"
    val BIN_01 = "Bin-01"
    val BIN_02 = "Bin-02"
    createInventoryHelper(TEST_SKU, BIN_01)

    val futureTrans = InventoryDoubleRecordDao.transfer(database, TEST_SKU, 2, BIN_01, BIN_02)
    val error = assertThrows[Exception](Await.result(futureTrans, Duration.Inf))

    val futureFind = InventoryDoubleRecordDao.findBySku(database, TEST_SKU)
    val findResult: Seq[InventoryDoubleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should equal(List(
      InventoryDoubleRecord(Some(1), "NewSku", 1, TRANSACTION.ADJUST, BIN_01),
    ))
  }

  test("findQty") {
    val TEST_SKU = "NewSku"
    val BIN_01 = "Bin-01"
    val BIN_02 = "Bin-02"
    createInventoryHelper(TEST_SKU, BIN_01)

    val qty: Option[Int] = Await.result(database.run(
      InventoryDoubleRecordDao.findQty(TEST_SKU, BIN_01).result), Duration.Inf)
    qty should equal(Option(1))

    createInventoryHelper(TEST_SKU, BIN_02)
    createInventoryHelper(TEST_SKU, BIN_01)
    createInventoryHelper(TEST_SKU, BIN_01)

    val qtyMultiple: Option[Int] = Await.result(database.run(
      InventoryDoubleRecordDao.findQty(TEST_SKU, BIN_01).result), Duration.Inf)
    qtyMultiple should equal(Option(3))
  }

  test("findAll") {
    val TEST_SKU = "NewSku"
    val BIN_01 = "Bin-01"
    val BIN_02 = "Bin-02"
    createInventoryHelper(TEST_SKU, BIN_01)
    createInventoryHelper(TEST_SKU, BIN_02)
    createInventoryHelper(TEST_SKU, BIN_01)
    createInventoryHelper(TEST_SKU, BIN_01)

    val futureFind = InventoryDoubleRecordDao.findAll(database)
    val findResult: Seq[InventoryDoubleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should equal(Vector(
      InventoryDoubleRecord(Some(1), TEST_SKU, 1, TRANSACTION.ADJUST, BIN_01),
      InventoryDoubleRecord(Some(2), TEST_SKU, 1, TRANSACTION.ADJUST, BIN_02),
      InventoryDoubleRecord(Some(3), TEST_SKU, 1, TRANSACTION.ADJUST, BIN_01),
      InventoryDoubleRecord(Some(4), TEST_SKU, 1, TRANSACTION.ADJUST, BIN_01),
    ))
  }
}