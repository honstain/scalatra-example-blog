package org.bitbucket.honstain.dao

import org.slf4j.{Logger, LoggerFactory}
import slick.jdbc.{PostgresProfile, TransactionIsolation}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

object TRANSACTION {
  val ADJUST = "adjust"
  val TRANSFER = "transfer"
}

//case class InventoryDoubleRecordLock(
//                                      location: String,
//                                      sku: String,
//                                      revision: Int,
//                                    )

class InventoryDoubleRecordLocks(tag: Tag) extends Table[(String, String, Int)](tag, "inventory_lock") {
  def location = column[String]("location")
  def sku = column[String]("sku")
  def revision = column[Int]("revision")

  def * = (location, sku, revision)
//  def * =
//    (location, sku, revision) <> (InventoryDoubleRecordLock.tupled, InventoryDoubleRecordLock.unapply)
}

case class InventoryDoubleRecord(
                                  id: Option[Int],
                                  sku: String,
                                  qty: Int,
                                  txnType: String,
                                  location: String
                                )

class InventoryDoubleRecords(tag: Tag) extends Table[InventoryDoubleRecord](tag, "inventory") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def sku = column[String]("sku")

  def qty = column[Int]("qty")

  def txnType = column[String]("type")

  def location = column[String]("location")

  def * =
    (id.?, sku, qty, txnType, location) <> (InventoryDoubleRecord.tupled, InventoryDoubleRecord.unapply)
}

object InventoryDoubleRecordDao extends TableQuery(new InventoryDoubleRecords(_)) {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  val insertAndGetId = this returning
    this.map(_.id) into ((inventory, id) => inventory.copy(id = Some(id)))

  def findAll(db: PostgresProfile.backend.DatabaseDef): Future[Seq[InventoryDoubleRecord]] = {
    db.run(this.result)
  }

  val groupByLocationSku: DBIO[Seq[(String, String, Int)]] =
    sql"""
           SELECT location, sku, SUM(qty)
           FROM inventory
           GROUP BY location, sku
      """.as[(String, String, Int)]

  def findAllFast(db: PostgresProfile.backend.DatabaseDef): Future[Seq[(String, String, Int)]] = {
    db.run(groupByLocationSku)
  }

  def findBySku(db: PostgresProfile.backend.DatabaseDef, sku: String): Future[Seq[InventoryDoubleRecord]] = {
    val findAction = this.filter(_.sku === sku)
    db.run(findAction.result)
  }

  def create(db: PostgresProfile.backend.DatabaseDef,
             sku: String,
             qty: Int,
             location: String
            ): Future[InventoryDoubleRecord] = {
    val findAction = insertAndGetId += InventoryDoubleRecord(Some(0), sku, qty, TRANSACTION.ADJUST, location)
    db.run(findAction)
  }

  def findQty(sku: String, location: String): Rep[Option[Int]] = {
    this.filter(x => x.sku === sku && x.location === location).map(_.qty).sum
  }

  def transfer(db: PostgresProfile.backend.DatabaseDef,
               sku: String,
               qty: Int,
               fromLocation: String,
               toLocation: String
              ): Future[Int] = {

    // Following the model from http://slick.lightbend.com/doc/3.3.0/dbio.html#transactions
    // Another reference https://stackoverflow.com/questions/30327617/slick-3-transactions
    //    val a = (for {
    //      n <- this += InventoryDoubleRecord(Some(0), sku, qty * -1, TRANSACTION.TRANSFER, fromLocation)
    //      m <- this += InventoryDoubleRecord(Some(0), sku, qty, TRANSACTION.TRANSFER, toLocation)
    //    } yield ()).transactionally

    //    val insert: DBIO[Option[Int]] = this ++= Seq(
    //      InventoryDoubleRecord(Some(0), sku, qty * -1, TRANSACTION.TRANSFER, fromLocation),
    //      InventoryDoubleRecord(Some(0), sku, qty, TRANSACTION.TRANSFER, toLocation),
    //    )

    // This example helped me pull this together http://queirozf.com/entries/slick-3-reference-and-examples
    val insert = for {
      lockRecord <- {
        val locks = TableQuery[InventoryDoubleRecordLocks]
        //locks.filter(x => x.location === fromLocation && x.sku === sku).forUpdate.result.head
        locks.filter(x => x.location === fromLocation && x.sku === sku).forUpdate.result
      }

      createUpdate <- {
        lockRecord match {
          case Seq((fromLocation, sku, _)) => {
            // Update
            val updateFoo = TableQuery[InventoryDoubleRecordLocks]
            val q = for { x <- updateFoo if x.location === fromLocation && x.sku === sku } yield x.revision
            q.update(lockRecord.head._3 + 1)
          }
          case _ => {
            // Create
            TableQuery[InventoryDoubleRecordLocks] += (fromLocation, sku, 0)
          }
        }
      }

      foo <- findQty(sku, fromLocation).result
      _ <- if (foo.get < 0) { //|| foo.get > 1) {
        logger.error(s"Invalid State - cannot transfer location:$fromLocation had:${foo.get}")
        DBIO.failed(new Exception("FAILED"))
      }
        else if (foo.get < qty) {
        logger.debug(s"Insufficient inventory to transfer location:$fromLocation had:${foo.get} but we needed:$qty")
        DBIO.failed(new Exception("FAILED"))
      } else {
        logger.debug(s"Found enough inventory to transfer, location:$fromLocation had:${foo.get} and we need:$qty")
        DBIO.successful(())
      }

      //_ <- DBIO.from(Future(logger.debug(s"Stand alone Found: ${foo.get}")))

      _ <- this += InventoryDoubleRecord(Some(0), sku, qty * -1, TRANSACTION.TRANSFER, fromLocation)
      bar <- this += InventoryDoubleRecord(Some(0), sku, qty, TRANSACTION.TRANSFER, toLocation)
    } yield (bar)

    db.run(insert.transactionally.withTransactionIsolation(TransactionIsolation.ReadCommitted))
    // These need to happen as a transaction
    // What should I return from this? both of the new records? or should I return nothing? A list of futures?
  }
}
