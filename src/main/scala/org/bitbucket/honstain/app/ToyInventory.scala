package org.bitbucket.honstain.app

import org.bitbucket.honstain.dao.{InventoryDoubleRecord, InventoryDoubleRecordDao, InventorySingleRecord, InventorySingleRecordDao}
import org.scalatra._
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success}
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}
// JSON handling support from Scalatra
import org.scalatra.json._

import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

class ToyInventory(database: PostgresProfile.backend.DatabaseDef) extends ScalatraServlet with JacksonJsonSupport {

  val logger: Logger = LoggerFactory.getLogger(getClass)
  protected implicit val jsonFormats: Formats = DefaultFormats

  //val dao = InventoryDoubleRecordDao
  val dao = InventorySingleRecordDao

  before() {
    contentType = formats("json")
  }

  def time[R](ident: String)(block: => R): R = {
    val t0 = System.currentTimeMillis()
    val result = block
    val t1 = System.currentTimeMillis()
    logger.debug(s"Time:${t1 - t0}ms $ident")
    result
  }

  get("/") {
    //val futureResult = time("SelectAll") { Await.result(dao.findAll(database), Duration.Inf) }

    val futureResult = time("SelectAll") { Await.result(dao.findAll(database), Duration.Inf) }
    //val futureResult = time("SelectAll") { Await.result(dao.findAllFast(database), Duration.Inf) }
    logger.debug(s"GET retrieved ${futureResult.size} inventory records")

    time("scalaLogic") {
//      // https://stackoverflow.com/questions/674639/scala-best-way-of-turning-a-collection-into-a-map-by-key
//      val foo: Map[(String, String), Seq[InventoryDoubleRecord]] = futureResult.groupBy(t => (t.sku, t.location))
//
//      def bar(input: Seq[InventoryDoubleRecord]): Int = input.foldLeft(0) { (acc, i) => acc + i.qty }
//
//      val foobar: Map[(String, String), Int] = foo mapValues bar
//      foobar.map { case (k, v) => Inventory(k._1, v, k._2) }

      futureResult.map { x => Inventory(x.sku, x.qty, x.location) }
      //futureResult.map { case (loc, sku, qty) => Inventory(sku, qty, loc) }
    }
    // How do I take this flat structure and consolidate it to a (location, sku) -> qty
//    val action = inventoryRecords.result
//    val result: Future[Seq[InventoryRecord]] = database.run(action)
//    val futureResult = Await.result(result, Duration.Inf)
//    //logger.debug(s"GET retrieved ${futureResult.size} inventory records")
//
//    futureResult.map(inv => Inventory(inv.sku, inv.qty, inv.location))
  }

  post("/") {
    val newInventory = parsedBody.extract[Inventory]
    logger.debug(s"Creating inventory sku:${newInventory.sku}")

    val future: Future[Option[InventorySingleRecord]] = dao.create(database, newInventory.sku, newInventory.qty, newInventory.location)
    future.onComplete {
      case Success(Some(value)) => logger.debug(s"Created new inventory record id:${value.id}")
      case Success(None) =>
        // Does it even make sense to try and account case like this?
        logger.error(s"Failed to create or update sku:${newInventory.sku}")
      case Failure(t) => logger.error(t.getMessage)
    }
    val result = Await.result(future, Duration.Inf)
    if (result.isDefined) {
      logger.debug(s"Created new inventory record id:${result.get}")
      // TODO - decide on more appropriate response data
      null
    }
    else {
      InternalServerError
    }
//    val action = insertQuery += InventoryRecord(Some(0), newInventory.sku, newInventory.qty, newInventory.description)
//    val result: Future[InventoryRecord] = database.run(action)
//    val futureResult = Await.result(result, Duration.Inf)
//    logger.debug(s"Created inventory with id:${futureResult.id} for sku:${newInventory.sku}")
//
//    if (futureResult.id.isDefined) {
//      val findAction =  inventoryRecords.filter(_.id === futureResult.id.get).result.head
//      val findResult: Future[InventoryRecord] = database.run(findAction)
//      val futureFindResult = Await.result(findResult, Duration.Inf)
//      Inventory(futureFindResult.sku, futureFindResult.qty, futureFindResult.description)
//    } else {
//      InternalServerError
//    }
  }

  post("/transfer") {
    val transfer = parsedBody.extract[InventoryTransfer]
    logger.debug(s"Transfer sku:${transfer.sku} from:${transfer.fromLocation} to:${transfer.toLocation}")

    val future: Future[Int] = dao.transfer(database, transfer.sku, transfer.qty, transfer.fromLocation, transfer.toLocation)
    future.onComplete {
      case Success(value) => logger.debug("Transfer transaction success")
      case Failure(t) => logger.debug(t.getMessage)
    }

    Await.result(future, Duration.Inf)
    // TODO - decide on more appropriate response data
    null
  }
}

case class Inventory(sku: String, qty: Int, location: String)
case class InventoryTransfer(sku: String, qty: Int, fromLocation: String, toLocation: String)